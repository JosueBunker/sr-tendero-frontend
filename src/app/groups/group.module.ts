import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { GroupRoutingModule } from './group-routing.module';
import { EventsModule } from '../events/events.module';
import { DevicesModule } from '../devices/devices.module';

import { GroupDetailComponent } from './group-detail/group-detail.component';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupPreviewComponent } from './group-list/group-preview/group-preview.component';
import { AddComponents } from './add-components/add-components.component';

import { CampaignPreviewComponent } from '../events/event-list/campaign-preview/campaign-preview.component';

@NgModule({
  declarations: [
    GroupDetailComponent,
    GroupListComponent,
    GroupPreviewComponent,
    AddComponents
  ],
  imports: [
    FormsModule,
    CommonModule,
    GroupRoutingModule,
    FontAwesomeModule,
    EventsModule,
    DevicesModule
  ]
})
export class GroupModule {}