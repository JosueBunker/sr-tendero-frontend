
import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { faArrowLeft, faSearch } from '@fortawesome/free-solid-svg-icons';

import { Constant } from '../../app.constants';

import { CampaignService } from '../../events/campaign.service';
import { DeviceService } from '../../devices/device.service';
import { GroupService } from '../group.service';

import { Campaign } from '../../models/campaign.model';
import { Device } from '../../models/device.model';
import { Group } from '../../models/group.model';

@Component({
  selector: 'app-add-components',
  templateUrl: './add-components.component.html',
  styleUrls: []
})
export class AddComponents implements OnInit {

  group: Group;
  type: string;

  CAMPAIGN = Constant.CAMPAIGN;
  DEVICE = Constant.DEVICE;

  selectedItems;
  notSelectedItems;
  isSelected;


  faArrowLeft = faArrowLeft;
  faSearch = faSearch;

  constructor(
    private _location: Location,
    private route: ActivatedRoute,
    private campaignService: CampaignService,
    private deviceService: DeviceService,
    private groupService: GroupService
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');  
    this.type = this.route.snapshot.paramMap.get('type');

    switch (this.type) {
      case this.CAMPAIGN:
        this.isSelected = this.isCampSelected;
        this.groupService.getCurrentGroup()
          .subscribe(group => {
            if (group == undefined)
              this.goBack();
            this.group = group;
            this.campaignService.getCampaigns()
              .subscribe(campaigns => {
                this.notSelectedItems = campaigns.filter(camp => !this.isSelected(camp));
              });
          });
        break;
      case this.DEVICE:
        this.isSelected = this.isDeviceSelected;
        this.groupService.getCurrentGroup()
          .subscribe(group => {
            if (group == undefined)
              this.goBack();
            this.group = group;
            this.deviceService.getDevices()
              .subscribe(devices => {
                this.notSelectedItems = devices.filter(devi => !this.isSelected(devi));
              });
          });
        break;
      default:
        this.goBack();
        break;
    }
  }
  
  onRemoveItem(id: number) {
    let temp;
    switch (this.type) {
      case this.CAMPAIGN:
        temp = this.group.campaigns.find(item => item.id == id);
        // this.group.campaigns = this.group.campaigns.filter(item => item.id != temp.id);
        this.group.removeCampaign(temp);
        break;
      case this.DEVICE:
        temp = this.group.devices.find(item => item.id == id);
        // this.group.devices = this.group.devices.filter(item => item.id != temp.id);
        this.group.removeDevice(temp);
        break;
      default:
        temp = undefined;
        break;
    }
    // if (this.type == this.CAMPAIGN) {
    //   temp = this.group.campaigns.find(item => item.id == id);
    //   this.group.campaigns = this.group.campaigns.filter(item => item.id != temp.id);
    // }
    if (temp)
      this.notSelectedItems.push(temp);
  }

  onAddItem(id: number) {
    const temp = this.notSelectedItems.find(item => item.id == id);

    this.notSelectedItems = this.notSelectedItems.filter(item => item.id != temp.id);
    switch (this.type) {
      case this.CAMPAIGN:
        this.group.addCampaign(temp);
        break;
      case this.DEVICE:
        this.group.addDevice(temp);
        break;
      default:
        break;
    }
    // if (this.type == this.CAMPAIGN)
    //   this.group.campaigns.push(temp);
  }

  isCampSelected(campaign) {
    console.log(this.selectedItems)
    for(let i = 0; i < this.group.campaigns.length; i++) {
      if (campaign.id == this.group.campaigns[i].id)
        return true;
    }

    return false;
  }

  isDeviceSelected(device) {
    for(let i = 0; i < this.group.devices.length; i++) {
      if (device.id == this.group.devices[i].id)
        return true;
    }
    return false;
  }

  goBack() {
    this._location.back();
  }
}