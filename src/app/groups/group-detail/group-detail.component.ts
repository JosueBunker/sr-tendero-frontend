import { Observable } from 'rxjs';

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { faArrowLeft, faPlus } from '@fortawesome/free-solid-svg-icons';

import { Group } from '../../models/group.model';
import { GroupService } from '../group.service';


@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {

  // @Input()
  group$: Observable<Group>;
  group: Group;

  faArrowLeft = faArrowLeft;
  faPlus = faPlus;

  showDeleteModal = false;

  constructor(
    private _location: Location,
    private groupService: GroupService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getGroup();
  }

  getGroup(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (isNaN(id)) {
      this.groupService.getCurrentGroup()
        .subscribe(group => {
          this.group = group;
        });
    } else {
      this.groupService.getGroup(id)
        .subscribe(group => {
          this.group = group;
        });
    }
  }

  onToggleDeleteModal() {
    this.showDeleteModal = !this.showDeleteModal;
  }

  goBack() {
    this.groupService.getDefaultGroup()
      .subscribe(group => {
        this._location.back();
      });
  }

  saveGroup() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (isNaN(id)) {
      this.groupService.postGroup(this.group)
        .subscribe(res => this.openSnackBar('Group Saved.'));
    } else {
      this.groupService.putGroup(this.group)
        .subscribe(res => this.openSnackBar('Group Saved.'));
    }
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  deleteGroup() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (!isNaN(id)) {
      this.groupService.deleteGroup(id)
        .subscribe(res => {
          this.openSnackBar('Group Deleted.');
          this._location.back();
        });
    }
  }
}