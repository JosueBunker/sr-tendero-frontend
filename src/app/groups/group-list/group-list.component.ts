import { Observable } from 'rxjs';

import { Component } from '@angular/core';

import { Group } from '../../models/group.model';
import { GroupService } from '../group.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent {

  groups$: Observable<Group[]>;


  constructor(
    private groupService: GroupService
  ) {}

  ngOnInit() {
    this.groups$ = this.groupService.getGroups();
  }

}