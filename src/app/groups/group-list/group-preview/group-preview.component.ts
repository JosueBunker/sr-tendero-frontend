import { Component, Input } from '@angular/core';

import { Group } from '../../../models/group.model';

@Component({
  selector: 'app-group-preview',
  templateUrl: './group-preview.component.html',
  styleUrls: ['./group-preview.component.css']
})
export class GroupPreviewComponent {

  @Input()
  group: Group;

}