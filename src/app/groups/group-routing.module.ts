import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GroupDetailComponent } from './group-detail/group-detail.component';
import { GroupListComponent } from './group-list/group-list.component';
import { AddComponents } from './add-components/add-components.component';

const groupRoutes: Routes = [
  { path: ':id/add-components/:type', component: AddComponents },
  { path: ':id', component: GroupDetailComponent },
  { path: '', component: GroupListComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(groupRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class GroupRoutingModule {}