import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';

import { Group } from '../models/group.model';
import { Campaign } from '../models/campaign.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  apiUrl = environment.apiUrl;
  currentGroup: Group;
  campaigns = [
    new Campaign({
      id: 1, 
      name: 'McDonalds', 
      media_url: 'https://res.cloudinary.com/dxickkr5f/image/upload/v1546617035/sr-tendero-ads/mcdonalds-logo.jpg', 
      media_type: 'Image', 
      status: true,
      date_start: new Date('2019-09-19 11:56:44'), 
      date_end: new Date('2019-11-19 11:56:44')
    }),
    new Campaign({
      id: 2, 
      name: 'ASDF', 
      media_url: 'https://res.cloudinary.com/dxickkr5f/image/upload/v1546617035/sr-tendero-ads/mcdonalds-logo.jpg', 
      media_type: 'Image', 
      status: true,
      date_start: new Date('2019-09-19 11:56:44'), 
      date_end: new Date('2019-11-19 11:56:44')
    })
  ];

  constructor (
    private http: HttpClient
  ) {}


  getDefaultGroup(): Observable<Group> {
    this.currentGroup = new Group({
        name: '',
        campaigns: [],
        devices: []
      });
    return Observable.create(observer => {
      observer.next(this.currentGroup);
    });
  }

  getCurrentGroup(): Observable<Group> {
    if (this.currentGroup == undefined) {
      this.currentGroup = new Group({
        name: '',
        campaigns: [],
        devices: []
      });
    }
    return Observable.create(observer => {
      observer.next(this.currentGroup);
    });
  }

  getGroups(): Observable<Group[]>{
    // let campaigns = this.campaigns;
    // return Observable.create(function (observer) {
    //   observer.next([new Group({id: 0, name: 'Zona 10', campaigns}), new Group({id: 1, name: 'Ciudad Guatemala', campaigns})])
    // });

    return this.http.get<Group[]>(this.apiUrl + '/group')
      .pipe(map(res => res.map(group => new Group(group))));
  }

  postGroup(group: Group) {
    console.log(group.toJSON());
    return this.http.post(this.apiUrl + '/group', group.toJSON())
  }

  putGroup(group: Group) {
    return this.http.put(this.apiUrl + '/group/' + String(group.id), group.toJSON())
  }

  deleteGroup(id: number): Observable<any> {
    return this.http.delete(this.apiUrl + '/group/' + String(id));
  }

  getGroup(id: number): Observable<Group> {
    if (this.currentGroup && this.currentGroup.id == id) {
      return Observable.create(observer => {
        observer.next(this.currentGroup);
        observer.complete();
      });
    } else {
      return this.http.get<Group>(this.apiUrl + '/group/' + String(id))
        .pipe(
          map(res => {
            let group = new Group(res);
            this.currentGroup = group;
            return group;
          })
        );
      // return Observable.create(observer => observer.next(new Group({0, 'Zona 10', campaigns)));
    }
  }
}