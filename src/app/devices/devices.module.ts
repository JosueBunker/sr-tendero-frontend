import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'

import { DevicesRoutingModule } from './devices-routing.module';

import { DeviceDetailComponent } from './device-detail/device-detail.component';
import { DeviceListComponent } from './device-list/device-list.component';
import { DevicePreviewComponent } from './device-list/device-preview/device-preview.component';

@NgModule({
  declarations: [
    DeviceListComponent,
    DevicePreviewComponent,
    DeviceDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DevicesRoutingModule,
    FontAwesomeModule
  ],
  exports: [
    DevicePreviewComponent
  ]
})
export class DevicesModule {}