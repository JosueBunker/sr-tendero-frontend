/// <reference types="@types/googlemaps" />

import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, distinct } from 'rxjs/operators';

import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { Device } from '../../models/device.model';
import { DeviceService } from '../device.service';

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements AfterViewInit {

  @ViewChild('gmap') gmapElement: any;
  @ViewChild('deviceForm') deviceForm;

  geocoder: google.maps.Geocoder;
  map: google.maps.Map;
  marker: google.maps.Marker;

  deviceIdSubject: Subject<string> = new Subject<string>();
  addressSubject: Subject<string> = new Subject<string>();

  device: Device;
  validDeviceId: boolean;

  faArrowLeft = faArrowLeft;

  showDeleteModal = false;
  
  constructor(
    private deviceService: DeviceService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _location: Location
  ) {}

  ngOnInit() {
    this.getDevice();
    this.geocoder = new google.maps.Geocoder;
    this.addressSubject
      .pipe(
        debounceTime(500),
        // distinctUntilChanged()
      )
      .subscribe(address => this.geocodeAddress(address));
  }

  ngAfterViewInit() {
    if (this.gmapElement !== undefined) {
      this.createMap();
    }
  }

  onToggleDeleteModal() {
    this.showDeleteModal = !this.showDeleteModal;
  }

  goBack() {
    this._location.back();
  }

  getDevice() {
    const id = this.route.snapshot.paramMap.get('id');
    // if (id === 'new') {
    this.deviceService.getDefaultDevice()
      .subscribe(device => this.device = device);
    if (!isNaN(+id)) {
      this.deviceService.getDevice(+id)
        .subscribe(device => {
          this.device = device;
          this.addMarker(new google.maps.LatLng(device.location_latitude, device.location_longitude));
        });
    }
  }

  saveDevice() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (isNaN(id)) {
      this.deviceService.postDevice(this.device)
        .subscribe(
          res => this.onSave(res),
          err => this.onError(err)
        );
    } else {
      this.deviceService.putDevice(this.device)
        .subscribe(
          res => this.onSave(res),
          err => this.onError(err)
        );
    }
  }

  onSave(res) {
    this.device.onSave()
    this.openSnackBar('Device Saved.');
  }

  onError(err) {
    this.openSnackBar('Device NOT saved')
  }

  deleteDevice() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deviceService.deleteDevice(id)
      .subscribe(res => {
        this.openSnackBar('Device Deleted.');
        this._location.back();
      });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 20000,
      panelClass: 'is-danger'
    });
  }

  onAddressChange($event) {
    this.device.setAddress($event);
    this.addressSubject.next($event);
  }

  createMap(): void {
    const mapCenter = {
      center: new google.maps.LatLng(14.634915, -90.506882),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapCenter);
    this.map.addListener('click', (event) => {
      this.addMarker(event.latLng);
    })
  }

  geocodeAddress(address: string) {
    this.geocoder.geocode({'address': address}, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        this.addMarker(results[0].geometry.location)
      }
    });
  }
  
  reverseGeocodingLatLng(latLng: google.maps.LatLng) {
    this.geocoder.geocode({'location': latLng}, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          console.log(results[0])
          // let pastType = results[0].address_components[0].types[0];
          // let address = results[0].address_components.map(addr => {
          //   if (addr.types
          // })
          // .reduce((acc, str) => acc + str);
          // console.log(address);
          this.device.address = results[0].formatted_address;
        } else {
          console.log('no results')
        }
      }
    });
  }


  addMarker(latLng: google.maps.LatLng) {
    if (this.marker != undefined) 
      this.marker.setMap(null);

    this.marker = new google.maps.Marker({
      position: latLng,
      map: this.map
    });
    
    this.device.location_latitude = latLng.lat();
    this.device.location_longitude = latLng.lng();
    
    this.map.panTo(latLng);

    this.reverseGeocodingLatLng(latLng);
  }
}
