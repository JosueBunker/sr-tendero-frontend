import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Device } from '../models/device.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {}

  getDevice(id: number): Observable<Device> {
    // return Observable.create(function(observer) {
    //   observer.next(new Device({id: 0, name: 'Los Proceres', address: 'zona 10', location_latitud: 24234234, location_longitud: 234242}));
    // });
    return this.http.get<Device>(this.apiUrl + '/device/' + String(id))
      .pipe(map(res => new Device(res)));
  }

  getDefaultDevice(): Observable<Device> {
    return Observable.create(function(observer) {
      observer.next(new Device({name: '', address: ''}))
    })
  }

  getDevices(): Observable<Device[]> {
    // return Observable.create(function(observer) {
    //   observer.next([
    //     new Device({id: 0, name: 'Los Proceres', address: 'zona 10', location_latitud: 24234234, location_longitud: 234242}), 
    //     new Device({id: 1, name: 'Arkadia', address: 'zona 10', location_latitud: 2400224, location_longitud: 2040234.23}), 
    //     new Device({id: 2, name: 'Oakland', address: 'zona 10', location_latitud: 230402, location_longitud: 2340202}), 
    //     new Device({id: 3, name: 'Cayala', address: 'zona 16', location_latitud: 400202, location_longitud: 20405})
    //   ]);
    // });

    return this.http.get<Device[]>(this.apiUrl + '/device')
      .pipe(map(res => res.map(devi => new Device(devi))));
  }

  postDevice(device: Device) {
    return this.http.post(this.apiUrl + '/device', device.toJSON())
      .pipe(map(res => console.log(res)));
  }

  putDevice(device: Device) {
    return this.http.put(this.apiUrl + '/device/' + String(device.id), device.toJSON()); 
  }

  deleteDevice(id: number) {
    return this.http.delete(this.apiUrl + '/device/' + String(id));
  }
}