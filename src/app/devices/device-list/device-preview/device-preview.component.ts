
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

import { Device } from '../../../models/device.model';

@Component({
  selector: 'app-device-preview',
  templateUrl: './device-preview.component.html',
  styleUrls: ['./device-preview.component.css']
})
export class DevicePreviewComponent implements OnInit {

  @Input() device: Device;
  @Input() onHoverClass?: string;

  @Output() callback?: EventEmitter<number> = new EventEmitter<number>();

  faPlus = faPlus;
  faTimes = faTimes;

  ngOnInit() {
    if (this.onHoverClass == undefined)
      this.onHoverClass = 'normal';
  }


  onClick() {
    if (this.callback)
      this.callback.emit(this.device.id);
  }
}