
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { Device } from '../../models/device.model';
import { DeviceService } from '../device.service';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: []
})
export class DeviceListComponent {

  devices$: Observable<Device[]>;

  constructor(
    private deviceService: DeviceService
  ) {}

  ngOnInit() {
    this.devices$ = this.deviceService.getDevices();
  }

  getDevices() {
    
  }
}