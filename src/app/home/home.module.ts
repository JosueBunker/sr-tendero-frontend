import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { EventListComponent } from '../events/event-list/event-list.component';
import { CampaignPreviewComponent } from '../events/event-list/campaign-preview/campaign-preview.component';

import { HomeRoutingModule } from './home-routing.module';
import { EventsModule } from '../events/events.module';
import { DevicesModule } from '../devices/devices.module';
import { StatsModule } from '../stats/stats.module';
import { CompanyModule } from '../company/company.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    // EventsModule,
    // DevicesModule,
    // StatsModule,
    // CompanyModule,
    HomeRoutingModule,
    // HomeRoutingModule,
    
    // DevicesModule,
  ]
})
export class HomeModule {}