import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { EventListComponent } from '../events/event-list/event-list.component';

import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  { path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'campaigns', loadChildren: () => import('../events/events.module').then(mod => mod.EventsModule) },
      { path: 'companies', loadChildren: () => import('../company/company.module').then(mod => mod.CompanyModule) },
      { path: 'devices', loadChildren: () => import('../devices/devices.module').then(mod => mod.DevicesModule) },
      { path: 'groups', loadChildren: () => import('../groups/group.module').then(mod => mod.GroupModule) },
      { path: 'stats', loadChildren: () => import('../stats/stats.module').then(mod => mod.StatsModule) }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule {}