import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register.component';
import { TempComponent } from './temp-comp/temp.component';

const routes: Routes = [
  // { 
  //   path: '',
  //   children: [
  //     { path: 'temp', component: TempComponent },
  //     { path: '', component: RegisterComponent }
  //   ]
  // },
  { path: ':id', component: TempComponent },
  { path: '', component: RegisterComponent },
  // { path: '', component:  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RegisterRoutingModule {}