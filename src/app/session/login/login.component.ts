import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup} from '@angular/forms';

import { faUser, faKey, faCircleNotch } from '@fortawesome/free-solid-svg-icons';

import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  faUser = faUser;
  faKey = faKey;
  faCircleNotch = faCircleNotch;

  loadingLogin: boolean = false;

  username: string;
  password: string;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {

  }

  prueba($event) {
    if ($event.key === 'Enter')
    console.log($event);
  }

  login() {
    this.loadingLogin = true;

    // console.log(this.username, this.password);
    this.authService.login(this.username, this.password)
      .subscribe(() => {
        if (this.authService.isLoggedIn) {
          let redirectUrl = this.authService.redirectUrl ? this.router.parseUrl(this.authService.redirectUrl) : '/';

          this.router.navigateByUrl(redirectUrl);
        } else {
          this.loadingLogin = false;
        }
      })
    
  }
}