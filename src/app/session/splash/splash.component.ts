
import { faCircleNotch} from '@fortawesome/free-solid-svg-icons';
import { Component } from '@angular/core'

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: []
})
export class SplashComponent {

  faCircleNotch = faCircleNotch;
}