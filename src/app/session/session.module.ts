import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { LoginComponent } from './login/login.component';
import { SessionComponent } from './session.component';
import { RegisterComponent } from './register/register.component';

import { SessionRouting } from './session-routing.module';
import { RegisterModule } from './register/register.module';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    SessionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    SessionRouting,
  ]
})
export class SessionModule {}