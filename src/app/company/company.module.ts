import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CompanyDetailComponent } from './company-detail/company-detail.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyPreviewComponent } from './company-list/company-preview/company-preview.component';

import { CompanyRoutingModule } from './company-routing.module';

import { EventsModule } from '../events/events.module';

@NgModule({
  declarations: [
    CompanyDetailComponent,
    CompanyListComponent,
    CompanyPreviewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CompanyRoutingModule,
    FontAwesomeModule,
    EventsModule,
  ]
})
export class CompanyModule {}