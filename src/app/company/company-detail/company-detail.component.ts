
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { Company } from '../../models/company.model';

import { CompanyService } from '../company.service';

@Component({
  selector: 'app-company-detail',
  templateUrl: 'company-detail.component.html',
  styleUrls: []
})
export class CompanyDetailComponent implements OnInit {

  company: Company;
  faArrowLeft = faArrowLeft;

  constructor(
    private companyService: CompanyService,
    private route: ActivatedRoute,
    private snackbar: MatSnackBar,
    private _location: Location
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id === 'new') {
      this.companyService.getDefaultCompany().
        subscribe(comp => {
          this.company = comp;
        });
    } else {
      this.companyService.getCompany(+id).
        subscribe(comp => {
          this.company = comp;
        });
    }
  }


  save() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (isNaN(id)) {
      this.companyService.postCompany(this.company)
        .subscribe(res => {
          this.openSnackBar('Company saved')
        })
    } else {

    }
  }

  openSnackBar(message: string) {
    this.snackbar.open(message, '', {
      duration: 2000,
    });
  }

  goBack() {
    this._location.back()
  }
}