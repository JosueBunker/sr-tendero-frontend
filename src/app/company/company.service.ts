
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { Company } from '../models/company.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  apiUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {}

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.apiUrl + '/company')
      .pipe(map(res => res.map(company => new Company(company))));
  }

  getDefaultCompany(): Observable<Company> {
    return Observable.create(observer => {
      observer.next(
        new Company({
          name: ''
        })
      );
    });
  }

  getCompany(id: number): Observable<Company> {
    return this.http.get<Company>(this.apiUrl + '/company/' + String(id))
      .pipe(map(res => new Company(res)));
  }

  postCompany(company: Company) {
    return this.http.post(this.apiUrl + '/company', company.toJSON())
  }
}