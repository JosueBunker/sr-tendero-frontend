import { Component, OnInit } from '@angular/core';


import { Company } from '../../models/company.model';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-company-list',
  templateUrl: 'company-list.component.html',
  styleUrls: []
})
export class CompanyListComponent implements OnInit {

  companies: Company[];

  constructor(
    private companyService: CompanyService
  ) {}
  
  ngOnInit() {
    this.companyService.getCompanies()
      .subscribe(companies => {
        this.companies = companies;
      });
  }

}