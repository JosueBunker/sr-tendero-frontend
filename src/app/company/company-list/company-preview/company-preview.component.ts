import { Component, Input } from '@angular/core';


import { Company } from '../../../models/company.model';

@Component({
  selector: 'app-company-preview',
  templateUrl: 'company-preview.component.html',
  styleUrls: []
})
export class CompanyPreviewComponent {
  @Input()
  company: Company;

  
}