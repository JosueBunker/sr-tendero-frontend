import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class AuthService {

  isLoggedIn = false;

  redirectUrl: string;

  login(username: string, password: string): Observable<boolean> {
    this.isLoggedIn = username === 'bunker' && password === 'bunker123';
    return of(this.isLoggedIn);
    // if (username === 'bunker' && password === 'bunker123') {
    //   return of(true);  
    // }
    // return of(false);
  }

  logout(): void {
    this.isLoggedIn = false;
  }
}