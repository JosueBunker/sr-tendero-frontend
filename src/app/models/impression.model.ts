
import * as moment from 'moment';

export class Impression {

  id: string;
  device_id: string;
  campaign_id: number;
  date_time: Date;

  constructor(obj: Partial<Impression>) {
    Object.assign(this, obj, {
      date_time: new Date(obj.date_time)
    });

    // console.log(this.date_time)
  }

  toJSON() {
    return Object.assign({}, this, {
      date: moment(this.date_time).format('YYYY-MM-DD hh:mm:ss')
    })
  }
}