
export class Device {
  id: number;

  device_id: string;
  prev_device_id: string;
  device_id_touched: boolean;

  name: string;
  prev_name: string;
  name_touched: boolean;

  address: string;
  prev_address: string;
  address_touched: boolean;

  location_latitude: number;
  prev_location_latitude: number;
  location_latitude_touched: boolean;

  location_longitude: number;
  prev_location_longitude: number;
  location_longitude_touched: boolean;

  touched: boolean;

  constructor(obj?: any) {
    Object.assign(this, obj, {
      touched: false,
      prev_device_id: obj.device_id,
      device_id_touched: false,
      prev_name: obj.name,
      name_touched: false,
      prev_address: obj.address,
      address_touched: false,
      prev_location_latitude: obj.location_latitude,
      location_latitude_touched: false,
      prev_location_longitude: obj.location_longitude,
      location_longitude_touched: false,
    });
  }

  setDeviceId(device_id: string) {
    this.device_id = device_id;

    this.device_id_touched = this.device_id !== this.prev_device_id;
    this.updateTouched();
  }

  setName(name: string) {
    this.name = name;

    this.name_touched = this.name !== this.prev_name;
    this.updateTouched();
  }

  setAddress(address: string) {
    this.address = address;

    this.address_touched = this.address !== this.prev_address;
    this.updateTouched();
  }

  setLocationLatitude(location_latitude: number) {
    this.location_latitude = location_latitude;

    this.location_latitude_touched = this.location_latitude !== this.prev_location_latitude;
    this.updateTouched;
  }

  setLocationLongitude(location_longitude: number) {
    this.location_longitude = location_longitude;

    this.location_longitude_touched = this.location_longitude !== this.prev_location_longitude;
    this.updateTouched();
  }

  isTouched(): boolean {
    return this.touched;
  }

  updateTouched() {
    this.touched = this.device_id_touched || this.name_touched || this.address_touched || this.location_latitude_touched || this.location_longitude_touched;
  }

  onSave() {
    this.prev_device_id = this.device_id;
    this.device_id_touched = false;

    this.prev_name = this.name;
    this.name_touched = false;

    this.prev_address = this.address;
    this.address_touched = false;

    this.prev_location_latitude = this.location_latitude,
    this.location_latitude_touched = false;

    this.prev_location_longitude = this.location_longitude;
    this.location_longitude_touched = false;

    this.updateTouched();
  }

  toJSON() {
    return Object.assign({}, this);
  }
}