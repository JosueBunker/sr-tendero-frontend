
import * as moment from 'moment';

export class Campaign {
  id: number;
  company_id: number;
  name: string;
  media_url: string;
  media_type: string;
  date_start: Date;
  date_end: Date;
  weight: number;
  seconds: number;
  status: boolean;
  total_impressions: number;
  current_impressions: number;

  constructor(obj?: Partial<Campaign>) {
    Object.assign(this, obj);

    if (!(this.date_start instanceof Date))
      this.date_start = new Date(this.date_start);
    if (!(this.date_end instanceof Date))
      this.date_end = new Date(this.date_end);
    if (!(typeof this.status === 'boolean'))
      this.status = this.status === 'active'? true : false;
  }

  public toJSON() {
    return Object.assign({}, this, {
      date_start: moment(this.date_start).format('YYYY-MM-DD hh:mm:ss'),
      date_end: moment(this.date_end).format('YYYY-MM-DD hh:mm:ss'),
      status: this.status === true? 'active' : 'inactive'
    });
  }

  public equal(other: Campaign): boolean {
    if (other == undefined) return false;

    let equal: boolean = true;
    if (other.id != this.id) equal = false;
    if (other.company_id != this.company_id) equal = false;
    if (this.name != this.name) equal = false;
    if (this.media_url != other.media_url) equal = false;
    if (this.media_type != other.media_type.slice(0, 10)) equal = false; 
    if (this.date_start.toString().slice(0, 10) != other.date_start.toString().slice(0, 10)) equal = false;
    if (this.date_end.toString().slice(0, 10) != other.date_end.toString().slice(0, 10)) equal = false;
    if (this.weight != other.weight) equal = false;

    return equal;
  }
}