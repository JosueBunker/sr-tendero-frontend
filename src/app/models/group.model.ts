
import {v4 as uuid} from 'uuid';

import { Campaign } from './campaign.model'
import { Device } from './device.model';

export class Group {
  // uuid: string;
  id: number;

  name: string;
  prev_name: string;
  name_touched: boolean;

  devices: Device[];
  prev_devices: Device[];
  devices_touched: boolean;
  
  campaigns: Campaign[];
  prev_campaigns: Campaign[];
  campaigns_touched: boolean;

  touched: boolean;

  constructor(obj?: any) {
    Object.assign(this, obj, {
      touched: false,
      prev_name: obj.name,
      devices_touched: false,
      campaigns_touched: false
    });

    let camps = []
    for (let i = 0; i < this.campaigns.length; i++) {
      camps.push(new Campaign(this.campaigns[i]));
    }
    this.campaigns = camps;
    this.prev_campaigns = this.campaigns.map(camp => camp);

    let devi = []
    for (let i = 0; i < this.devices.length; i++) {
      devi.push(new Device(this.devices[i]));
    }
    this.devices = devi;
    this.prev_devices = this.devices.map(devi => devi);
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;

    console.log(name);

    this.name_touched = this.name !== this.prev_name;
    this.updateTouched();
  }

  isTouched(): boolean {
    return this.touched;
  }

  updateTouched() {
    this.touched = this.name_touched || this.campaigns_touched || this.devices_touched;
    console.log(this.touched);
  }

  addCampaign(campaign: Campaign) {
    this.campaigns.push(campaign);

    this.campaigns_touched = !this.arrayEquals(this.campaigns, this.prev_campaigns);
    this.updateTouched();
  }

  removeCampaign(campaign: Campaign) {
    this.campaigns = this.campaigns.filter(camp => camp.id != campaign.id);

    this.campaigns_touched = !this.arrayEquals(this.campaigns, this.prev_campaigns);
    this.updateTouched();
  }

  addDevice(device: Device) {
    this.devices.push(device);

    this.devices_touched = !this.arrayEquals(this.devices, this.prev_devices);
    this.updateTouched();
  }

  removeDevice(device: Device) {
    this.devices = this.devices.filter(devi => devi.id != device.id);

    this.devices_touched = !this.arrayEquals(this.devices, this.prev_devices);
    this.updateTouched();
  }


  toJSON() {
    return Object.assign({}, this, {
      devices: this.devices.map(device => device.id),
      campaigns: this.campaigns.map(campaign => campaign.id)
    });
  }

  arrayEquals(array1, array2): boolean {
    return (array1.length === array2.length) && array1.every((ele, index) => ele.id === array2[index].id); 
  }
}