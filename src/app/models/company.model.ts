
import { Campaign } from './campaign.model';

export class Company {

  id: number;
  name: string;
  campaigns: Campaign[];

  constructor(obj?: Partial<Company>) {
    Object.assign(this, obj, {
      campaigns: obj.campaigns? obj.campaigns.map(camp => new Campaign(camp)) : []
    });
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }

  getCampaigns(): Campaign[] {
    return this.campaigns;
  }

  addCampaign(temp: Campaign) {
    this.campaigns.push(temp);
  }

  removeCampaign(temp: Campaign) {
    this.campaigns.filter(camp => camp.id === temp.id);
  }

  toJSON() {
    return Object.assign({}, this);
  }
}