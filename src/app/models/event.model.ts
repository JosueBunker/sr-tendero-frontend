
import { Image } from './image.model';

export class Event {
  id: number;
  name: string;
  startDate: string;
  endDate: string;
  imagesId: number[];
  images: Image[];
  active: boolean;

  constructor(id: number, name: string, startDate: string, endDate: string) {
    this.id = id;
    this.name = name;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public getSimpleStartDate() {
    return this.startDate.split(" ")[0];
  }

  public getSimpleEndDate() {
    return this.endDate.split(" ")[0];
  }
}