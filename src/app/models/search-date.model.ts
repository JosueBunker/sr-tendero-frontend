
import * as moment from 'moment';

export class SearchDate {

  partialDate: Date;
  validDate: boolean;

  hour: number;
  validHour: boolean;

  minute: number;
  validMinute: boolean;

  valid: boolean;

  constructor(obj: Partial<SearchDate>) {
    Object.assign(this, obj, {
      validDate: false,
      validMinute: false,
      validHour: false,
      partialDate: null,
      hour: null,
      minute: null,
      valid: true
    });
  }

  private updateValid() {
    if (this.partialDate == null && this.hour == null && this.minute == null) {
      this.valid = true;
    } else {
      this.valid = this.validDate && this.validHour && this.validMinute;
    }
  }

  isValid() {
    return this.valid;
  }

  getPartialDate(): Date {
    return this.partialDate;
  }

  setPartialDate(partialDate: Date) {
    this.partialDate = partialDate;
    this.validDate = partialDate != null;
    this.updateValid();
  }

  getHour(): number {
    return this.hour;
  }

  setHour(hour: number): void {
    this.hour = hour;
    this.validHour = this.hour >= 0 && this.hour <= 24 && this.hour != null;
    this.updateValid();
  }

  getMinute(): number {
    return this.minute;
  }

  setMinute(minute: number): void {
    this.minute = minute;
    this.validMinute = this.minute >= 0 && this.minute <= 60 && this.minute != null;
    this.updateValid();
  }


  getDate() {
    if (this.partialDate == null && this.hour == null && this.minute == null) {
      return undefined;
    }
    return moment(this.partialDate).add(this.hour, 'hours').add(this.minute, 'minutes').format('YYYY-MM-DD hh:mm:ss');
  }
}