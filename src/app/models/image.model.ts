
export class Image {
  id: number;
  name: string;
  uri: string;
  created_at: string;
  categories: string[];
  active: boolean;

  constructor(id: number, name: string, uri: string, categories: string[]) {
    this.id = id;
    this.name = name;
    this.uri = uri;
    this.categories = categories;
    this.active = true;
  }
}