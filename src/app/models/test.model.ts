
export class Test {

  name: string;
  safo: string;

  constructor(obj?: Partial<Test>) {

    let { name, safo } = obj;
    this.name = name;
    this.safo = safo;
  }

  printName() {
    console.log(this.name);
  }
}