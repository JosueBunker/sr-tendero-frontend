
import { ChartsModule } from 'ng2-charts';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule, MatSnackBarModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CampaignPreviewComponent } from './events/event-list/campaign-preview/campaign-preview.component';

import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { SessionModule } from './session/session.module';
import { ImagesModule } from './images/images.module';
import { EventsModule } from './events/events.module';
import { GroupModule } from './groups/group.module';
import { DevicesModule } from './devices/devices.module';
import { StatsModule } from './stats/stats.module';
// import { FiledropDirective } from './filedrop.directive';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    // HomeModule,
    HttpClientModule,
    // EventsModule,
    // GroupModule,
    // StatsModule,
    // DevicesModule,
    // ImagesModule,
    // SessionModule,
    AppRoutingModule,
    FontAwesomeModule,
    NoopAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(router: Router) {
    // const replacer = (key, value) => (typeof value === 'function') ? value.name: value;
    // console.log('Routes: ', JSON.stringify(router.config, replacer, 2));
  }
}
