import { Component } from '@angular/core';
import { Router } from '@angular/router';

// import { ImageService } from './image.service';
import { Image } from './models/image.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  images = [];

  constructor() {

  }

  // ngOnInit() {
  //   this.imageService.getImagesAPI()
  //     .subscribe((data: Image) => console.log(data));
  // }

  // getImages() {
  //   this.images = this.imageService.getImages();
  //   console.log('clic');
  // }
}
