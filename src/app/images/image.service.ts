import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Image } from '../models/image.model';

import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ImageService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  // getImages(): Image[] {
  //   return [new Image(1, 'Logo Coca Cola', 'https://res.cloudinary.com/dxickkr5f/image/upload/v1546617035/sr-tendero-ads/cocacola-logo.jpg', ['ropa', 'comida'])]
  // }


  getImages() {
    return this.http.get<Image[]>(this.apiUrl + '/images');
  }

  getImage(id: string) {
    return this.http.get<Image>(this.apiUrl + '/image/' + id);
  }
}
