import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageListComponent } from './image-list/image-list.component';
import { ImageDetailComponent } from './image-detail/image-detail.component';

const imagesRoutes: Routes = [
  { path: 'image/:id', component: ImageDetailComponent },
  { path: 'images', component: ImageListComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(imagesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ImagesRoutingModule { }