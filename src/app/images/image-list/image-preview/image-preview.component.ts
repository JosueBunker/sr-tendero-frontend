import { Component, Input } from '@angular/core';

import { Image } from '../../../models/image.model';



@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.css']
})
export class ImagePreviewComponent {
  @Input()
  image: Image;

  onNgInit() {
    console.log(this.image);
  }

}