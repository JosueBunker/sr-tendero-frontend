
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ImageService } from '../image.service';
import { Image } from '../../models/image.model';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent implements OnInit {

  images$: Observable<Image[]>;
  
  constructor(
    private service: ImageService,
    private route: ActivatedRoute
  ) {}
  
  ngOnInit() {
    this.getImages();
  }

  getImages() {
    this.images$ = this.route.paramMap.pipe(
      switchMap(params => this.service.getImages())
    );

    this.images$.subscribe(value => console.log(value))
  }
}