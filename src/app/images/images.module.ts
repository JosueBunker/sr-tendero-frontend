import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ImageDetailComponent } from './image-detail/image-detail.component';
import { ImageListComponent } from './image-list/image-list.component';
import { ImagePreviewComponent } from './image-list/image-preview/image-preview.component';

import { ImagesRoutingModule } from './images-routing.module';

@NgModule({
  declarations: [
    ImageDetailComponent,
    ImageListComponent,
    ImagePreviewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ImagesRoutingModule
  ]
})
export class ImagesModule {}