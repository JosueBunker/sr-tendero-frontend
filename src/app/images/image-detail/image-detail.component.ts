import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

import { ImageService } from '../image.service';
import { Image } from '../../models/image.model';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.css']
})
export class ImageDetailComponent implements OnInit {

  image$: Observable<Image>;

  constructor(
    private service: ImageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.image$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getImage(params.get('id')))
    );

    this.image$.subscribe(value => console.log(value));
  }

  gotoImages() {
    this.router.navigate(['/images']);
  }
}
