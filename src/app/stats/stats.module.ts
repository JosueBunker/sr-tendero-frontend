
import { ChartsModule } from 'ng2-charts';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import { CampaignStats } from './campaign-stats/campaign-stats.component';
import { StatOverview } from './stat-overview/stat-overview.component';

import { StatsRoutingModule } from './stat-routing.module';

@NgModule({
  declarations: [
    CampaignStats,
    StatOverview
  ],
  imports: [
    CommonModule,
    FormsModule,
    StatsRoutingModule,
    ChartsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    FontAwesomeModule
  ]
})
export class StatsModule {}