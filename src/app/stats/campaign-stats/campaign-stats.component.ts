
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { faArrowLeft, faCircleNotch, faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

import { StatService } from '../stat.service';

import { Campaign } from '../../models/campaign.model';
import { Device } from '../../models/device.model';
import { Impression } from '../../models/impression.model';
import { SearchDate } from '../../models/search-date.model';

@Component({
  selector: 'app-campaign-stats',
  templateUrl: './campaign-stats.component.html',
  styleUrls: ['./campaign-stats.component.css']
})
export class CampaignStats {

  faArrowLeft = faArrowLeft;
  faCircleNotch = faCircleNotch;
  faSearch = faSearch;
  faTimes = faTimes;

  campaign: Campaign;
  impressions: Impression[];
  devices: Device[];

  startDate: SearchDate;
  endDate: SearchDate;

  constructor(
    private _location: Location,
    private route: ActivatedRoute,
    private statService: StatService
  ) {}

  ngOnInit() {
    this.startDate = new SearchDate({});
    this.endDate = new SearchDate({});
    const id = +this.route.snapshot.paramMap.get('id');

    this.statService.getCampaign(id)
      .subscribe(camp => {
        this.campaign = camp;
        this.statService.getImpressions(this.getParams())
          .subscribe(impressions => {
            console.log(impressions.length);
            this.impressions = impressions;
          });
      });
  }

  onClearStartDate() {
    this.startDate.setPartialDate(null);
  }

  onClearStartTime() {
    this.startDate.setHour(null);
    this.startDate.setMinute(null);
  }

  onClearEndDate() {
    this.endDate.setPartialDate(null);
  }

  onClearEndTime() {
    this.endDate.setHour(null);
    this.endDate.setMinute(null);
  }

  getParams() {
    return {
      campaign_id: this.campaign.id
    }
  }

  goBack() {
    return this._location.back();
  }
}