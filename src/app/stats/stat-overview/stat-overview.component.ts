
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as moment from 'moment';

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { faCircleNotch, faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

import { StatService } from '../stat.service';

import { Impression } from '../../models/impression.model';
import { SearchDate } from '../../models/search-date.model';

@Component({
  selector: 'app-stat-overview',
  templateUrl: './stat-overview.component.html',
  styleUrls: ['./stat-overview.component.css']
})
export class StatOverview implements OnInit {

  faCircleNotch = faCircleNotch;
  faSearch = faSearch;
  faTimes = faTimes;


  campaigns;

  pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formater: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    },
  };
  pieChartLabels: Label[] = ['Label 1', 'Label 2', 'Label 3'];
  pieChartData: number[] = [300, 500, 100];
  pieChartType: ChartType = 'pie';
  pieChartLegend = true;
  // pieChartColors = [
  //    {
  //      backgroundColor: ['rgba(255, 0, 0, 0.3)', 'rgba(0, 255, 0, 0.3)', 'rbga(0, 0, 255, 0.3)'],
  //    }
  //  ];

  startDate: SearchDate;
  endDate: SearchDate;

  impressions: Impression[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private statService: StatService
  ) {}

  ngOnInit() {
    this.startDate = new SearchDate({});
    this.endDate = new SearchDate({});
    this.statService.getCampaigns()
      .subscribe(campaigns => {
        console.log(campaigns);
        this.campaigns = campaigns;
        this.pieChartLabels = campaigns.map(camp => camp.name);
        this.pieChartData = campaigns.map(camp => camp.current_impressions);
      });
    this.statService.getImpressions({})
      .subscribe(impressions => {
        this.impressions = impressions;
      });
  }

  onClearStartDate() {
    this.startDate.setPartialDate(null);
  }

  onClearStartTime() {
    this.startDate.setHour(null);
    this.startDate.setMinute(null);
  }

  onClearEndDate() {
    this.endDate.setPartialDate(null);
  }

  onClearEndTime() {
    this.endDate.setHour(null);
    this.endDate.setMinute(null);
  }

  goToCampaignStats(id: number) {
    this.router.navigate(['campaign/' + id], { relativeTo: this.route })
    console.log(id);
  }

  onSearch() {
    let params;
      params = {
        date_from: this.startDate.getDate(),
        date_to: this.endDate.getDate()
      }
    
    this.statService.getImpressions(params)
      .subscribe(impressions => {
        this.impressions = impressions;
      });
      
    }
}