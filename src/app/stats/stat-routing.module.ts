import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CampaignStats } from './campaign-stats/campaign-stats.component';
import { StatOverview } from './stat-overview/stat-overview.component';

const statsRoutes: Routes = [
  { path: 'campaign/:id', component: CampaignStats },
  { path: '', component: StatOverview }
];

@NgModule({
  imports: [
    RouterModule.forChild(statsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class StatsRoutingModule {}