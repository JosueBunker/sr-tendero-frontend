
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Campaign } from '../models/campaign.model';
import { Impression } from '../models/impression.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  apiUrl = environment.apiUrl;

  currentCampaign: Campaign;

  constructor(
    private http: HttpClient
  ) {}


  getDefaultImpression(): Observable<Impression[]> {
    return Observable.create(observer => {
      observer.next([new Impression({})])
    })
  }

  getCampaign(id: number): Observable<Campaign> {
    if (this.currentCampaign && this.currentCampaign.id == id){
      return Observable.create(observer => {
        observer.next(this.currentCampaign);
      });
    } else {
      return this.http.get<Campaign>(this.apiUrl + '/campaign/' + String(id))
        .pipe(
          map(res => {
            let camp = new Campaign(res)
            this.currentCampaign = camp;
            return camp;
          })
        );
    }
  }

  getCampaigns(): Observable<Campaign[]> {
    return this.http.get<Campaign[]>(this.apiUrl + '/campaign')
      .pipe(map(res => res.map(camp => new Campaign(camp))));
  }

  getImpressions(params: any): Observable<Impression[]> {
    return this.http.get<Impression[]>(this.apiUrl + '/impression', {params: params})
      .pipe(map(res => res.map(imp => new Impression(imp))));
  }
}