
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';

import { CampaignService } from './campaign.service';
import { Campaign } from '../models/campaign.model';

@Injectable({
  providedIn: 'root',
})
export class CrisisDetailResolverService implements Resolve<Campaign>{

  constructor(
    private campaignService: CampaignService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Campaign> | Observable<never> {
    let id = +route.paramMap.get('id');

    return this.campaignService.getCampaign(id)
      .pipe(
        take(1),
        mergeMap(campaign => {
          if (campaign) {
            return of(campaign);
          } else {
            this.router.navigate(['/home/campaigns']);
            return EMPTY;
          }
        })
      );
  }
}