import { from, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Event } from '../models/event.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getEvents() {
    // return this.http.get<Event[]>(this.apiUrl +  '/events');
    // return from ([new Event(1, 'mes 2', '2019/03/01 11:03', '2019/04/01 11:03'), new Event(1, 'mes 3', '2019/04/01 11:03', '2019/05/01 11:03')]);
    return Observable.create(function(observer) {
      observer.next([new Event(1, 'mes 2', '2019/03/01 11:03', '2019/04/01 11:03'), new Event(1, 'mes 3', '2019/04/01 11:03', '2019/05/01 11:03')]);
      observer.complete()
    });
  }
}