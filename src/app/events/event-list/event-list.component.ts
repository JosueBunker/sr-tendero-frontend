import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { Event } from '../../models/event.model';
import { EventService } from '../event.service';

import { Image } from '../../models/image.model';
import { ImageService } from '../../images/image.service';

import { Campaign } from '../../models/campaign.model';
import { CampaignService } from '../campaign.service';

@Component({
  selector: 'app-event-list',
  host: {
    class: 'is-full-width'
  },
  templateUrl: 'event-list.component.html',
  styleUrls: ['event-list.component.css']
})
export class EventListComponent implements OnInit {
  
  events$: Observable<Event[]>;
  images$: Observable<Image[]>;
  campaigns$: Observable<Campaign[]>;

  constructor(
    private service: EventService,
    private imageService: ImageService,
    private campaignService: CampaignService
  ) {}

  ngOnInit() {
    this.images$ = this.imageService.getImages();
    this.events$ = this.service.getEvents();
    this.campaigns$ = this.campaignService.getCampaigns();
    // this.campaigns$.subscribe(value => console.log(value));
  }

  // createNewCampaign() {
  //   newCamp = new Campaign(0, "", "https://res.cloudinary.com/dxickkr5f/image/upload/v1558717023/sr-tendero-ads/bunker_name.png", 'image', 'active')
  // }
}