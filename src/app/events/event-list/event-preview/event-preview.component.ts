import { Component, Input } from '@angular/core';

import { Event } from '../../../models/event.model';

@Component({
  selector: 'app-event-preview',
  templateUrl: './event-preview.component.html',
  styleUrls: ['./event-preview.component.css']
})
export class EventPreviewComponent {
  @Input()
  event: Event;

  
}