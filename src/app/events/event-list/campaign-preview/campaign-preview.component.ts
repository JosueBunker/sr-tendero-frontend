import { Component, Input, Output, EventEmitter } from '@angular/core';
import { faArrowLeft, faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

import { Campaign } from '../../../models/campaign.model';



@Component({
  selector: 'app-campaign-preview',
  templateUrl: 'campaign-preview.component.html',
  styleUrls: ['campaign-preview.component.css']
})
export class CampaignPreviewComponent {
  @Input()
  campaign: Campaign;
  @Input()
  onHoverClass?: string;
  @Input() height?: number;

  @Output()
  callback: EventEmitter<number> = new EventEmitter<number>();
  
  faArrowLeft = faArrowLeft;
  faPlus = faPlus;
  faTimes = faTimes;

  ngOnInit() {
    if (this.onHoverClass == undefined)
      this.onHoverClass = 'normal';
    if (this.height == undefined)
      this.height = 330;
  }

  onClick() {
    this.callback.emit(this.campaign.id);
  }
}