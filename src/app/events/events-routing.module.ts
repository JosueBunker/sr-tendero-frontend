import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EventListComponent } from './event-list/event-list.component';
import { CampaignDetailComponent } from './campaign-detail/campaign-detail.component';

const eventsRoutes: Routes = [
  // { 
  //   path: '',
  //   children: [
  //     { path: '', component: EventListComponent, outlet: 'home' },
  //     { path: ':id', component: CampaignDetailComponent, outlet: 'home' }
  //   ]
  // },
  { path: ':id', component: CampaignDetailComponent },
  { path: '', component: EventListComponent },
  // { path: '', component: EventListComponent, outlet: 'home' },
  // { path: 'home/campaign/:id', component: CampaignDetailComponent, outlet: 'home' },
  // { path: 'campaign/:id', component: CampaignDetailComponent, outlet: 'home' },
  // { path: 'home/events', component: EventListComponent, outlet: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forChild(eventsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EventsRoutingModule {}