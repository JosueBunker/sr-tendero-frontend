import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { EventListComponent } from './event-list/event-list.component';
import { EventPreviewComponent } from './event-list/event-preview/event-preview.component';

import { CampaignDetailComponent } from './campaign-detail/campaign-detail.component';
import { CampaignPreviewComponent } from './event-list/campaign-preview/campaign-preview.component'
import { FileDropDirective } from './file-drop.directive';

import { EventsRoutingModule } from './events-routing.module';

@NgModule({
  declarations: [
    EventListComponent,
    EventPreviewComponent,
    CampaignDetailComponent,
    CampaignPreviewComponent,
    FileDropDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    EventsRoutingModule,
    FontAwesomeModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],  
  exports: [
    CampaignPreviewComponent
  ]
})
export class EventsModule {}