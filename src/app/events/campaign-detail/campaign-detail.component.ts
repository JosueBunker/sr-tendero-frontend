import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { faCloudUploadAlt, faArrowLeft, faCircleNotch } from '@fortawesome/free-solid-svg-icons';


import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { HttpEventType } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


import { Campaign } from '../../models/campaign.model';
import { Company } from '../../models/company.model';
import { Upload } from '../../models/upload.model';
import { CampaignService } from '../campaign.service'


import * as _ from 'lodash';


@Component({
  selector: 'app-campaign-detail',
  templateUrl: 'campaign-detail.component.html',
  styleUrls: ['campaign-detail.component.css']
})
export class CampaignDetailComponent implements OnInit {

  @ViewChild('calendar') calendar;

  campaign$: Observable<Campaign>;
  campaign: Campaign;
  camp: Campaign;
  newCamp: Campaign;
  companies: Company[];

  campaignForm: FormGroup;

  loading: boolean = false;

  formChanged: boolean = false;
  currentUpload: Upload;
  dropzoneActive: boolean = false;
  uploadPercentage: number = 0;

  showDeleteModal = false;

  faCloudUpload = faCloudUploadAlt;
  faArrowLeft = faArrowLeft;
  faCircleNotch = faCircleNotch;

  constructor(
    private route: ActivatedRoute,
    private service: CampaignService,
    private formBuilder: FormBuilder,
    private _location: Location,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.campaignForm = this.formBuilder.group({
      id: new FormControl(''),
      name: new FormControl('', Validators.required),
      company_id: new FormControl(''),
      media_url: new FormControl('', Validators.required),
      media_type: new FormControl('image', Validators.required),
      date_start: new FormControl('', Validators.required),
      date_end: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      seconds: new FormControl('', Validators.required),
      total_impressions: new FormControl('', Validators.required),
      current_impressions: new FormControl('')
    });
    this.getCampaign();
    // this.campaign$.subscribe(campaign => {
      // this.onCampaignChange(campaign)
      // this.campaignForm.patchValue(campaign);
      // this.camp = campaign;
      // console.log(campaign.date_start);
      // this.campaignForm = this.formBuilder.group({
      //   id: new FormControl(campaign.id),
      //   name: new FormControl(campaign.name, Validators.required),
      //   company_id: new FormControl(campaign.company_id),
      //   media_url: new FormControl(campaign.media_url, Validators.required),
      //   media_type: new FormControl(campaign.media_type, Validators.required),
      //   start_date: new FormControl(campaign.date_start),
      //   end_date: new FormControl(campaign.date_end),
      //   status: new FormControl(campaign.status),
      // });

      // this.campaignForm.valueChanges.subscribe(data => {
      //   console.log(this.campaignForm.valid)
      //   let camp = this.camp;
      //   console.log(this.campaignForm.status);
      //   console.log(JSON.stringify(data));
      //   console.log(JSON.stringify(this.camp));
      //   if (JSON.stringify(data) != JSON.stringify(this.camp)) {
      //     this.formChanged = true;
      //   } else
      //     this.formChanged = false;
      //   console.log(this.formChanged)
      //   // this.newCamp = data as Campaign;
      // });
    // });

  }

  onCampaignChange(campaign: Campaign) {
    this.camp = campaign;
    this.campaign = campaign;
    console.log(campaign.date_start);
    
    this.campaignForm.valueChanges.subscribe(data => {
      // let camp = this.camp;
      // console.log(this.campaignForm.status);
      console.log(data);
      // if (JSON.stringify(data) != JSON.stringify(this.camp)) {
      //   this.formChanged = true;
      // } else
      //   this.formChanged = false;
      // console.log(this.formChanged)
      this.campaign = new Campaign(data);
    });
  }

  getCampaign(): void {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    if (id === 'new') {
      this.service.getDefaultCampaign().subscribe(campaign => {
        this.onCampaignChange(campaign);
        this.service.getCompanies()
          .subscribe(companies => {
            this.companies = companies;
          });
      });
    } else {
      this.service.getCampaign(+id)
        .subscribe(campaign => {
          this.campaignForm.setValue(campaign);
          this.onCampaignChange(campaign);
        });
    }
  }

  saveCampaign() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (isNaN(id)) {
      // console.log(this.newCamp.toJSON())
      this.service.postCampaign(this.campaign)
        .subscribe(res => this.openSnackBar('Campaign Saved.'));
    } else {
      this.service.saveCampaign(this.campaign)
        .subscribe(() => this.openSnackBar('Campaign Saved.'));
    }
  }

  onToggleDeleteModal() {
    this.showDeleteModal = !this.showDeleteModal;
  }



  deleteCampaign() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (!isNaN(id)) {
      this.service.deleteCampaign(id)
        .subscribe(res => {
          this.openSnackBar('Campaign Deleted.')
          this._location.back();
        });
    }
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  goBack() {
    this._location.back();
  }

  dropzoneState($event: boolean) {
    this.dropzoneActive = $event;
  }

  inputChanged(type, $event) {
    const dat: Date = new Date($event.target.value);

    console.log(dat)
  }

  onSubmit() {
    console.log(this.campaignForm.value);
  }

  handleFileUpload($event) {
    let fileList: FileList = $event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.set('media', file, file.name);

      this.loading = true;
      this.service.pushUpload(formData)
        .subscribe(event => {
          if (event.type == HttpEventType.UploadProgress) {
            this.uploadPercentage = Math.round(100 * event.loaded / event.total);
            console.log(`File is ${this.uploadPercentage}% uploaded`);
          } else if (event.type == HttpEventType.Response) {
            console.log(event.body);

            this.campaignForm.patchValue({ media_url: event.body['url'] });
            // this.onCampaignChange(new Campaign({...this.newCamp, media_url: event.body['url']}))
            this.loading = false;
          }

        });
    }
  }

  uploadFile(file: File) {
    let formData:FormData = new FormData();
    formData.set('media', file, file.name);

    this.loading = true;
    this.service.pushUpload(formData)
      .subscribe(event => {
        console.log(event)
        if (event.type == HttpEventType.UploadProgress) {
          this.uploadPercentage = Math.round(100 * event.loaded / event.total);
          console.log(`File is ${this.uploadPercentage}% uploaded`);
        } else if (event.type == HttpEventType.Response) {
          console.log(event.body);

          // this.onCampaignChange(new Campaign({...this.campaign, media_url: event.body['url']}))
          this.loading = false;
        }

      });
  }

  handleDrop(fileList: FileList) {
    let filesIndex = _.range(fileList.length);

    console.log(fileList)
    this.uploadFile(fileList[0]);
    // _.each(filesIndex, (idx) => {
    //   this.currentUpload = new Upload(fileList[idx]);
    //   this.service.pushUpload(this.currentUpload);
    // })
  }
}