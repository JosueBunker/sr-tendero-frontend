
import { from, Observable, of } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';

import { Campaign } from '../models/campaign.model';
import { Company } from '../models/company.model';
import { Upload } from '../models/upload.model';
import { Test } from '../models/test.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  apiUrl = environment.apiUrl;

  headers: HttpHeaders;

  currentCampaign: Campaign;


  constructor(
    private http: HttpClient
  ) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-type', 'multipart/form-data');
    this.headers.append('Accpet', 'application/json');
  }

  getDefaultCampaign(): Observable<Campaign> {
    return Observable.create(function (observer) {
      observer.next(
        new Campaign({
          name: '', 
          media_url: '', 
          media_type: 'image', 
          status: true,
          date_start: new Date(), 
          date_end: new Date()
        })
      );
    });
  }

  getCampaign(id: number): Observable<Campaign> {
    return this.http.get<Campaign>(this.apiUrl + '/campaign/' + String(id))
      .pipe(map(res => {
        let camp = new Campaign(res)
        console.log(camp);
        return camp;
      }));
  }

  getCompany(id: number): Observable<Company> {
    return this.http.get<Company>(this.apiUrl + '/company/' + String(id))
      .pipe(map(res => new Company(res)));
  }

  postCampaign(campaign: Campaign) {
    console.log(campaign);
    return this.http.post(this.apiUrl + '/campaign', campaign.toJSON());
  }

  saveCampaign(campaign: Campaign) {
    return this.http.put(this.apiUrl + '/campaign/' + String(campaign.id), campaign.toJSON())
      .pipe(
        tap(_ => console.log('saved hero')),
        catchError(this.handleError<any>('saveCampaign'))
      );
  }

  deleteCampaign(id: number) {
    return this.http.delete(this.apiUrl + '/campaign/' + String(id));
  }

  getCampaigns(): Observable<Campaign[]> {
    return this.http.get<Campaign[]>(this.apiUrl + '/campaign')
      .pipe(map(res => res.map(camp => new Campaign(camp))));
  }

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.getCompany +  '/company?clean=true')
      .pipe(map(res => res.map(comp => new Company(comp))));
  }



  pushUpload(formData: FormData) {
    console.log(formData)
    let req =  new HttpRequest('POST', this.apiUrl + '/upload/media', formData, {
      reportProgress: true
    })
    return this.http.request(req);
  }

  handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      return of(result as T);
    }
  }
}